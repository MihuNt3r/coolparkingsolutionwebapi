﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Services;
using CoolParking.WebAPI.Interfaces;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet]
        public ActionResult GetVehicles()
        {
            return Ok(_parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult GetVehicleById(string id)
        {
            if (!Settings.regex.IsMatch(id))
                return BadRequest("Invalid id");

            Vehicle vehicle = _parkingService.GetVehicles().ToList().Find(v => v.Id == id);

            if (vehicle == null)
                return NotFound("Can't find car with such id");

            return Ok(vehicle);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Vehicle> PostVehicle(Vehicle vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
                return Created("", vehicle);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult RemoveVehicle(string id)
        {
            if (!Settings.regex.IsMatch(id))
                return BadRequest("Invalid id");

            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
