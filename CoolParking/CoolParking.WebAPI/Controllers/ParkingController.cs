﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.WebAPI.Services;
using CoolParking.WebAPI.Interfaces;
using System.Threading.Tasks;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;
        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet("balance")]
        public decimal GetBalance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public int GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public int GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}
