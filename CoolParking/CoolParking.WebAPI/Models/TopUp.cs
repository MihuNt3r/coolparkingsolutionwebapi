﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class TopUp
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("Sum")]
        public decimal Sum { get; }

        public TopUp(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }
    }
}
