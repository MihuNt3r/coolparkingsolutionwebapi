﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class Parking : IDisposable
    {
        public decimal Balance { get; private set; }
        public List<Vehicle> Vehicles { get; }
        public List<TransactionInfo> LastParkingTransactions { get; private set; }

        private static Parking instance;

        private Parking()
        {
            Balance = Settings.startingBalance;
            Vehicles = new List<Vehicle>();
            LastParkingTransactions = new List<TransactionInfo>();
        }

        public static Parking GetInstance()
        {
            if (instance == null)
                instance = new Parking();

            return instance;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count >= Settings.parkingCapacity)
                throw new InvalidOperationException("Parking is full");

            if (Vehicles.Count != 0 && Vehicles.Any(v => vehicle.Id == v.Id))
                throw new ArgumentException("Id already exists");

            Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = Vehicles.Find(vehicle => vehicle.Id == vehicleId);

           if (vehicle == null)
               throw new ArgumentException("Can't find vehicle with such id");

           if (vehicle != null && vehicle.Balance < 0)
               throw new InvalidOperationException("Cannot remove a vehicle, because it has a debt");

           Vehicles.Remove(vehicle);
        }

        public void Withdraw()
        {
            if (Vehicles.Count != 0)
            {
                foreach (Vehicle vehicle in Vehicles)
                {
                    decimal withdrawSum = CalculateWithdrawSum(vehicle);
                    vehicle.Balance -= withdrawSum;
                    this.Balance += withdrawSum;
                    TransactionInfo info = new TransactionInfo(DateTime.Now, vehicle.Id, withdrawSum);
                    LastParkingTransactions.Add(info);
                }
            }
        }

        private decimal CalculateWithdrawSum(Vehicle vehicle)
        {
            decimal tariff = Settings.tariffs[vehicle.VehicleType];
            decimal withdrawSum;

            if (vehicle.Balance > 0 && vehicle.Balance >= tariff)// if balance is greater than 0 and greater than tariff just assign tariff to withdrawSum
            {
                withdrawSum = tariff;
            }
            else if (vehicle.Balance <= 0)// if balance is 0 or lower - calculate tariff with fineCoefficient multiplier
            {
                withdrawSum = tariff * Settings.fineCoefficient;
            }
            else// if balance is greater than 0 but lower than tariff - calculate remainder with fineCoefficient multiplier
            {
                decimal remainder = tariff - vehicle.Balance;
                remainder *= Settings.fineCoefficient;
                withdrawSum = vehicle.Balance + remainder;
            }

            return withdrawSum;
        }

        public void TopUp(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Invalid topup sum");

            var vehicle = Vehicles.Find(vehicle => vehicle.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Can't find vehicle with such number");

            vehicle.Balance += sum;
        }
        public TransactionInfo[] GetLastTransactions()
        {
            return LastParkingTransactions.ToArray();
        }

        public void ClearTransactionList()
        {
            LastParkingTransactions.Clear();
        }

        public void Dispose()
        {
            if (instance != null)
            {
                instance.Balance = 0;
                instance.Vehicles.Clear();
                instance.LastParkingTransactions.Clear();
            }
        }
    }
}
