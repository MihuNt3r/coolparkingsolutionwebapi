﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}
