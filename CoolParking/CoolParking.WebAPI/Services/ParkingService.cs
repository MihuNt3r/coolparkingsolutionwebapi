﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking parking;
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private readonly ILogService logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.parking = Parking.GetInstance();
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;

            this.withdrawTimer.Elapsed += Withdraw; // Add handler for withdrawing
            this.logTimer.Elapsed += WriteToLog; // Add handler for logging

            this.withdrawTimer.Start();
            this.logTimer.Start();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            parking.AddVehicle(vehicle);
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
            parking.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.parkingCapacity - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return parking.GetLastTransactions();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            parking.RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            parking.TopUp(vehicleId, sum);
        }
        // Handler method for withdrawing 
        private void Withdraw(Object source, ElapsedEventArgs e)
        {
            parking.Withdraw();
        }
        // Handler method for writing to log
        private void WriteToLog(Object source, ElapsedEventArgs e)
        {
            TransactionInfo[] lastTransactions = GetLastParkingTransactions(); // Retrieve last transactions
            string logInfo = null;
            for (int i = 0; i < lastTransactions.Length; i++)
            {
                logInfo += lastTransactions[i].ToString() + "\n";
            }
            logService.Write(logInfo); // Write last transactions to log
            parking.ClearTransactionList(); // Clear transaction list after writing to log
        }
    }
}
