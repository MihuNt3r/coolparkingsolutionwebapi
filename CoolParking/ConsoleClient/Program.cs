﻿using System;
using System.Threading.Tasks;
using CoolParking.WebAPI.Models;


namespace ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Menu menu = new Menu();
            await menu.Start();
        }
    }
}
